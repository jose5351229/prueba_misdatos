# prueba_misdatos.
# INSTALACIÓN DEL PROYECTO EN UBUNTU.
# 1. CLONAR REPOSITORIO.
    git clone https://gitlab.com/jose5351229/prueba_misdatos.git
# 2. INSTALAR PYTHON3.8 AND VIRTUALENV.
    sudo apt install python3.8
    pip3.8 install virtualenv
# 3. CREAR AMBIENTE.
    virtualenv <env-name> -p python3.8
    source <env-path>/bin/activate
# 4. INSTALAR REQUIREMENTS.
    pip install -r requirements.txt
# 4.1 En caso de no poder instalar psycopg2 en ubuntu.servicio
    sudo apt install libpq-dev python3-dev
    sudo apt install build-essential
# 5. DATABASE (asegurarse de tener configurada la base de datos).

CONECCION:

    DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'pruebatecnica',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': 5432
    },

COMANDOS PARA MIGRAR:

    python manage.py makemigrations
    python manage.py migrate
# 6. INSERCION INFORMACION EN LA DATABASE.
Los datos que tengan la bd son los que proporciona la api para el primer punto
# 7. SE CORRE EL SERVICIO DE DJANGO.
    python manage.py runserver 8000
# 8. 1(PRIMER) PUNTO.
Abrir el archivo punto1.html
# 9. 2(SEGUNDO) PUNTO.
Ejecutar el script punto2.py

    python3.8 punto2.py

# 10. ARCHIVOS ADJUNTOS.
Archivo 'Test de Liderazgo JOSE RAMIREZ.docx',
Archivo 'casos de usos.docx',
Imagen 'Diagrama de entidades.jpg',
Imagen 'Diagrama De Secuencia General.jpg',
Archivo 'WARTEGG 8 CAMPOS - HOJA DE RESPUESTAS.pdf'.
