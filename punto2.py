from bs4 import BeautifulSoup
import requests
import json
import webbrowser

url = 'https://webscraper.io/test-sites/e-commerce/scroll'
page = requests.get(url)
soup = BeautifulSoup(page.content, 'html.parser')

items_title = soup.find_all('a', class_='title', limit=10)
items_price = soup.find_all('h4', class_='price', limit=10)
items_description = soup.find_all('p', class_='description', limit=10)

items = []

itera = max(len(items_price), len(items_title), len(items_description))

for index in range(0, itera):
    item = {
                'title': items_title[index].text,
                'price': items_price[index].text,
                'description': items_description[index].text,
            }
    items.append(item)

with open('media/items.json', 'w') as file:
    json.dump(items, file, indent=4)

webbrowser.open_new_tab('media/items.json')
webbrowser.open_new_tab('punto2_template.html')
