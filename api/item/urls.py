from django.urls import path
from .views import ItemView, ScrapingItemView

urlpatterns = [
    path('items/', ItemView.as_view(), name='item_list'),
    path('items/<int:id>', ItemView.as_view(), name='item_process'),
    path('items_punto_dos/', ScrapingItemView.as_view(), name='item_punto_dos_list')
]