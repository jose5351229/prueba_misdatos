import json

from django.http import JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from api.settings import BASE_DIR
from item.models import Item

# Create your views here.


class ItemView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):
        if id > 0:
            items = list(Item.objects.filter(id=id).values())
            if len(items) > 0:
                item = items[0]
                datos = {'message': 'Succes', 'item': item}
            else:
                datos = {'message': 'Item not found...'}
            return JsonResponse(datos)
        else:
            items = list(Item.objects.values())
            if len(items) > 0:
                datos = {'message': 'Succes', 'items': items}
            else:
                datos = {'message': 'Items not found...'}
            return JsonResponse(datos)

    def post(self, request):
        jd = json.loads(request.body)
        Item.objects.create(nombre=jd['nombre'], medida=jd['medida'], precio_bisel=jd['precio_bisel'],
                            precio_sin_bisel=jd['precio_sin_bisel'], color=jd['color'], stock=jd['stock'],
                            tiempo_entrega=jd['tiempo_entrega'])
        datos = {'message': 'Succes'}
        return JsonResponse(datos)

    def put(self, request, id):
        jd = json.loads(request.body)
        items = list(Item.objects.filter(id=id).values())
        if len(items) > 0:
            item=Item.objects.get(id=id)
            item.nombre=jd['nombre']
            item.medida=jd['medida']
            item.precio_bisel=jd['precio_bisel']
            item.precio_sin_bisel=jd['precio_sin_bisel']
            item.color=jd['color']
            item.stock=jd['stock']
            item.tiempo_entrega=jd['tiempo_entrega']
            item.save()
            datos = {'message': 'Succes'}
        else:
            datos = {'message': 'Item not found...'}
        return JsonResponse(datos)

    def delete(self, request, id):
        items = list(Item.objects.filter(id=id).values())
        if len(items) > 0:
            Item.objects.filter(id=id).delete()
            datos = {'message': 'Succes'}
        else:
            datos = {'message': 'Item not found...'}
        return JsonResponse(datos)


class ScrapingItemView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        data = []
        with open(str(BASE_DIR)+'/../media/items.json') as file:
            data = json.load(file)
        if len(data) > 0:
            datos = {'message': 'Succes', 'items': data}
        else:
            datos = {'message': 'Items not found...'}
        return JsonResponse(datos)