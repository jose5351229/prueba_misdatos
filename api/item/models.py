from django.db import models

# Create your models here.
class Item(models.Model):
    nombre = models.CharField(max_length=100)
    medida = models.CharField(max_length=50)
    precio_bisel = models.PositiveIntegerField()
    precio_sin_bisel = models.PositiveIntegerField()
    color = models.CharField(max_length=100)
    stock = models.PositiveIntegerField()
    tiempo_entrega = models.PositiveIntegerField()

